import home from "./home";
import locale from "./locale";
import navigation from "./navigation";
import termsAndConditions from "./termsAndConditions";

export {
    home,
    locale,
    navigation,
    termsAndConditions,
};
