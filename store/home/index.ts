import { action, computed, observable } from "mobx";

interface IHomeStore {
    openSignin?: boolean;
    openRegistration?: boolean;
    openMobileMenu?: boolean;
}

export class HomeStoreClass implements IHomeStore {

    @observable public openSignin: boolean = false;
    @observable public openRegistration: boolean = false;
    @observable public openMobileMenu: boolean = false;
    @observable public openForgotPassword: boolean = false;
    @observable public showBack: boolean = true;

    constructor(){
        this.setSignin = this.setSignin.bind(this);
        this.setRegistration = this.setRegistration.bind(this);
        this.setMobileMenu = this.setMobileMenu.bind(this);
        this.setForgotPassword = this.setForgotPassword.bind(this);
        this.setShowBack = this.setShowBack.bind(this);
    }

    @computed public get getSignin(): boolean {
        return this.openSignin;
    }

    @computed public get getRegistration(): boolean {
        return this.openRegistration;
    }

    @computed public get getMobileMenu(): boolean {
        return this.openMobileMenu;
    }

    @computed public get getForgotPassword(): boolean {
        return this.openForgotPassword;
    }

    @action public setSignin(status: boolean): void {
        this.openSignin = status;
        this.openRegistration = status === true ? false : this.openRegistration;
    }

    @action public setRegistration(status: boolean): void {
        this.openSignin = status === true ? false : this.openSignin;
        this.openRegistration = status;
    }

    @action public setMobileMenu(status: boolean): void {
        this.openMobileMenu = status;
    }

    @action public setForgotPassword(status: boolean): void {
        this.openForgotPassword = status;
        this.openSignin  = !status;
    }

    @action public setShowBack(status: boolean): void {
        this.showBack = status;
    }

}

export const HomeStore = new HomeStoreClass();
export default HomeStore;
