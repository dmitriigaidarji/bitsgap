import {ErrorComponent} from "_component/ErrorComponent";
import LazyLoadComponent from "_component/LazyLoadComponent";
import {LoadingComponent} from "_component/LoadingComponent";
declare const System: { import: (path: string) => Promise<any>; };

let FooterSection: any;
let HeaderInfoBlock: any;
if (process.env.BROWSER) {
   FooterSection = LazyLoadComponent(() => System.import("./FooterSection"), LoadingComponent, ErrorComponent);
   HeaderInfoBlock = LazyLoadComponent(() => System.import("./HeaderInfoBlock"), LoadingComponent, ErrorComponent);
} else {
   FooterSection = require("./FooterSection").default;
   HeaderInfoBlock = require("./HeaderInfoBlock").default;
}
export {
    FooterSection,
    HeaderInfoBlock,
};
