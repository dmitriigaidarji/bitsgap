import {IHeaderInfo} from "_block/HeaderInfoBlock/interface";
import NavigationComponent from "_component/NavigationComponent";
import {getStyle, initStyle} from "_utils/rBem";
import * as React from "react";

export class HeaderInfoBlock extends React.Component<IHeaderInfo, {width?: number, height?: number}> {

    constructor(props) {
        super(props);
        this.state = { width: 0, height: 0 };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    public componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener("resize", this.updateWindowDimensions);
    }

    public componentWillUnmount() {
        window.removeEventListener("resize", this.updateWindowDimensions);
    }

    public updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    public render() {
        return (
            <div>
                <div className={initStyle(getStyle("section"), getStyle("header-info"), getStyle("header-info", "lite"))}>
                    <NavigationComponent/>
                </div>
            </div>
        );
    }
}

export default HeaderInfoBlock;
