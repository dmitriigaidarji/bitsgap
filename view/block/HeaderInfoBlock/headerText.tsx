import ButtonComponent from "_component/ButtonComponent";
import {HomeStoreClass} from "_store/home";
import {getStyle, initStyle} from "_utils/rBem";
import {inject} from "mobx-react";
import * as React from "react";
import {RouterStore} from "mobx-react-router";
(window as any).forceSmoothScrollPolyfill = true;

@inject("home", "routing")
export class HeaderText extends React.Component<{mobile?: boolean, home?: HomeStoreClass, routing?: RouterStore}, {}> {

    constructor(props) {
        super(props);
        this.onOpenRegistration = this.onOpenRegistration.bind(this);
        this.openMarketCap = this.openMarketCap.bind(this);
    }

    public render() {
        const {mobile} = this.props;
        return (
            <div className={getStyle("header-text", "info-block")}>
                <div className={getStyle("header-text", "title")}>
                    Trade on all major crypto exchanges<br/>within one platform
                </div>
                <ul className={getStyle("header-text", "capabilities")}>
                    <li>
                        <span>20+ crypto exchanges including Binance, Poloniex, Kraken, Gate.io, Kucoin</span>
                    </li>
                    <li>
                        <span>4400+ Crypto Pairs | Demo Trading | Portfolio | 1-Click Arbitrage</span>
                    </li>
                </ul>
                <div className={getStyle("header-text", "sign-block")}>
                    {!mobile && <ButtonComponent title={"Sign up now"} className={getStyle("header-text", "signup")} onClick={this.onOpenRegistration}/>}
                    <ButtonComponent onClick={() => {document.getElementById("featuresSelectionPage-1").scrollIntoView({ behavior: "smooth", block: "start"}) }} title={"Learn more"} className={getStyle("header-text", "learn")}/>
                    <ButtonComponent onClick={this.openMarketCap} title={"Market cap"} className={initStyle(getStyle("header-text", "learn"), getStyle("header-text", "learn", "market-cap"))}/>
                    <ButtonComponent onClick={() => {this.props.home.setSignin(true)}} title={"Login"} className={initStyle(getStyle("header-text", "learn"), getStyle("header-text", "learn", "market-cap"))}/>
                    <ButtonComponent onClick={this.onOpenRegistration} title={"Sign up now"} className={initStyle(getStyle("header-text", "learn"), getStyle("header-text", "learn", "sign-up"))}/>

                    {/*removed buttons for ios, android apps temporarily:*/}

                    {/*{mobile && <ButtonComponent title={""} className={getStyle("header-text", "android")} style={{backgroundImage: `url(${require("_images/android_badge.png")})`}}/>}*/}
                    {/*{mobile && <ButtonComponent title={""} className={getStyle("header-text", "mac")} style={{backgroundImage: `url(${require("_images/mac_badge.svg")})`}}/>}*/}
                </div>
            </div>
        );
    }

    public openMarketCap() {
        this.props.routing.push("/market-cap");
    }

    private onOpenRegistration() {
        const {setRegistration} = this.props.home;
        setRegistration(true);
    }

}

export default HeaderText;
