import ButtonComponent from "_component/ButtonComponent";
import {LanguageMenuComponent} from "_component/LanguageMenuComponent";
import LinkComponent from "_component/LinkComponent";
import LogoComponent from "_component/LogoComponent";
import {INavigationInterface} from "_component/NavigationComponent/interface";
import {HomeStoreClass} from "_store/home";
import LocaleStore from "_store/locale";
import {INavItem, NavigationClass} from "_store/navigation";
import {getStyle, initStyle} from "_utils/rBem";
import {inject, observer, Provider} from "mobx-react";
import {RouterStore} from "mobx-react-router";
import * as React from "react";

const NavigationItem: React.SFC<INavItem> = ({href, title, clicked, active, target, ...props}) => {
    active = href === window.location.pathname;
    return (
       <li
           className={initStyle(getStyle("header-navigation-list", "item"), active && getStyle("header-navigation-list", "item", "active"))}
           onClick={clicked}
       >
        <LinkComponent href={href} title={title} target={target} {...props}/>
       </li>
   );
};

@inject("navigation", "home", "routing")
@observer
class NavigationListComponent extends React.Component<{navigation?: NavigationClass, home?: HomeStoreClass, routing?: RouterStore}, {}> {

    constructor(props) {
        super(props);
        this.toggleLocalization = this.toggleLocalization.bind(this);
    }

    public render() {
        const {getList, getActiveIndex, getShowLanguages} = this.props.navigation;
        const {history} = this.props.routing;
        return (
            <div className={getStyle("header-navigation-menu")}>
            <ul className={initStyle(getStyle("header-navigation-list"), getStyle("navigation", "computer-navigation"))}>
                {getList.map((props, key) => {
                    return <NavigationItem key={key} {...props} active={key === getActiveIndex} clicked={this.updateActive.bind(this, key, props.href, props.target)} />;
                })}
            </ul>
                <div className={initStyle(getStyle("header-navigation", "buttons"), getStyle("header-navigation", "computer-buttons"))}>
                    <ButtonComponent title={"login"} className={getStyle("header-navigation", "login-button")}/>
                    <ButtonComponent title={"sign up"} className={getStyle("header-navigation", "signin-button")}
                                     onClick={()=>{history.push("sign-up")}}
                    />
                    <Provider locale={LocaleStore}>
                        <LanguageMenuComponent show={getShowLanguages} toggle={this.toggleLocalization}/>
                    </Provider>
                </div>
            </div>
        );
    }

    private toggleLocalization() {
        const {getShowLanguages, setShowLanguages} = this.props.navigation;
        setShowLanguages(!getShowLanguages);
    }

    private updateActive(index, href, target) {
        const {routing} = this.props;
        this.props.navigation.setActiveIndex(index);
        if (!target) {
            routing.push(href);
        }else{
            window.open(href, target);
        }
    }

}

export class NavigationComponent extends React.Component<INavigationInterface, {}> {

    public static defaultProps = {
        className: getStyle("navigation"),
    };

    public render() {
        return (
            <div>
                <div className={getStyle("home-desktop")}>
                    <div className={getStyle("header-navigation")}>
                        <a href="/"><LogoComponent/></a>
                        <NavigationListComponent/>
                    </div>
                </div>
            </div>
        );
    }

}

export default NavigationComponent;
