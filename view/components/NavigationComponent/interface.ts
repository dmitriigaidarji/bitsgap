import {classNames} from "_style";
import {HomeStoreClass} from "_store/home";

export interface INavigationInterface {
    home?: HomeStoreClass;
    className?: classNames;
}
