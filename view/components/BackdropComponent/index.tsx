
import {getStyle, initStyle} from "_utils/rBem";
import {inject, observer} from "mobx-react";
import * as React from "react";
import {Fade} from "react-reveal";
import {IBackdropComponent} from "./interface";

@inject("home")
@observer
export class BackdropComponent extends React.PureComponent<IBackdropComponent, {}> {

    constructor(props) {
        super(props);
        this.onBackDropClicked = this.onBackDropClicked.bind(this);
    }

    public render() {

        const {className, home} = this.props;
        const classes = initStyle(getStyle("back-drop"), className);
        const show = home.getRegistration || home.getSignin || home.openMobileMenu;

        return (
            <div onClick={this.onBackDropClicked} style={{display: show ? "block" : "none"}}>
                {show && <Fade className={classes}><span/></Fade>}
            </div>
        );
    }

    private onBackDropClicked() {
        const {home, hideOnClick} = this.props;
        if (hideOnClick) {
            home.setSignin(false);
            home.setRegistration(false);
            home.setMobileMenu(false);
        }
    }
}

export default BackdropComponent;
