import {getStyle, initStyle} from "_utils/rBem";
import {inject, observer} from "mobx-react";
import {RouterStore} from "mobx-react-router";
import * as React from "react";

@inject("routing")
@observer
export class TopMenuComponent extends React.Component<{ routing?: RouterStore }, {}> {

    public render() {
        const {routing} = this.props;
        const onTrading = routing.location.pathname === "/trading";
        // const navigationStyle = "-on-trading";
        return (
            <div style={{left: !onTrading ? "188px" : ""}} className={initStyle(getStyle("top-navigation", "wrapper"))}>
                <nav className={getStyle("top-navigation", "nav")}>{}</nav>
            </div>
        );
    }
}

export default TopMenuComponent;
