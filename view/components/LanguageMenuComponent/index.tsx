import ButtonComponent from "_component/ButtonComponent";
import {ILocaleItem, LocaleStoreClass} from "_store/locale";
import {getStyle, initStyle} from "_utils/rBem";
import {inject, observer} from "mobx-react";
import * as React from "react";
import {Zoom} from "react-reveal";

@inject("locale")
@observer
export class LanguageMenuComponent extends React.Component<{locale?: LocaleStoreClass, show?: boolean, toggle?: () => void}, {}> {

    public wrapperRef: any;

    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.changeLocale = this.changeLocale.bind(this);
        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    public componentDidMount() {
        document.addEventListener("mousedown", this.handleClickOutside);
    }

    public componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleClickOutside);
    }

    public render() {
        const {getLocale, getLocales} = this.props.locale;
        const selected = getLocale.key.toUpperCase();
        const localeStyle = ({key}) => initStyle(getStyle("dropdown", "item"), getLocale.key === key && getStyle("dropdown", "item", "active"));
        return (
            <div className={getStyle("header-navigation", "localization")} ref={this.setWrapperRef}>>
                <ButtonComponent title={selected} icon={"chevron-down"} iconPosition={"right"} className={initStyle(getStyle("header-navigation", "localization-button"), this.props.show && getStyle("header-navigation", "localization-button", "show"))} onClick={this.toggle}/>
                <div className={getStyle("dropdown")}>
                    {this.props.show && <Zoom duration={350}>
                        <div className={getStyle("dropdown", "container")}>
                            {getLocales.map((locale) => (
                                <div key={`locale-${locale.key}`} className={localeStyle(locale)} onClick={this.changeLocale.bind(this, locale)}>{locale.key.toUpperCase()}</div>
                            ))}
                        </div>
                    </Zoom>}
                </div>
            </div>
        );
    }

    private setWrapperRef(node) {
        this.wrapperRef = node;
    }

    private handleClickOutside(event) {
        if (this.props.show && this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.toggle();
        }
    }

    private changeLocale(locale: ILocaleItem) {
        const {setLocale} = this.props.locale;
        setLocale(locale);
    }

    private toggle() {
        this.props.toggle();
    }

}
