import ImageComponent from "_component/ImageComponent";
import {ILogoInterface} from "_component/LogoComponent/interface";
import {getStyle, initStyle} from "_utils/rBem";
import * as React from "react";

export class LogoComponent extends React.Component<ILogoInterface, {}> {

    public static defaultProps = {
        className: getStyle("logo"),
        logoName: "logo",
    };

    constructor(props: ILogoInterface) {
        super(props);
    }

    public render() {

        const {className, logoName} = this.props;
        const classes = initStyle(getStyle("logo"), className);

        return (
            <div className={classes}>
                <ImageComponent alt={"Bitsgap"} name={logoName} type={"png"} />
            </div>
        );
    }

}

export default LogoComponent;
