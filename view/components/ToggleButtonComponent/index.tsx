import {getStyle, initStyle} from "_utils/rBem";
import * as React from "react";

export class ToggleButtonComponent extends React.Component<{checked?: boolean, changeView?: any, disabled?: boolean}> {
    public state = {
        checked: false,
    }

    constructor(props: any) {
        super(props);
        this.toggle = this.toggle.bind(this);
    }

    public setInitialState(checked?: boolean) {
        this.setState({
            checked: typeof checked === "undefined" ? this.props.checked : checked,
        });
    }

    public componentWillReceiveProps(nextProps){
        this.setInitialState(nextProps.checked);
    }

    public componentDidMount(){
        this.setInitialState();
    }

    public toggle() {
        if (!this.props.disabled) {
            this.setState({
                checked: !this.state.checked,
            });
            this.props.changeView(!this.state.checked);
        }
    }

    public render() {
        return(
            <div className={initStyle(getStyle("toggle-button"), this.props.disabled && getStyle("toggle-button", null, "disabled"))}>
                <label htmlFor={"tunnels"} id={"tunnels"} onClick={this.toggle} className={getStyle("toggle-button", "switch")}>
                    <input id={"tunnels"} type="checkbox" checked={this.state.checked}/>
                        <span className={initStyle(getStyle("toggle-button", "slider"), getStyle("toggle-button", "slider", this.state.checked ? "checked" : ""))}></span>
                </label>
            </div>

        );
    }
}

export default ToggleButtonComponent;
