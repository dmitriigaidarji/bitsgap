import TopMenuComponent from "_component/TopMenuComponent";
import {getStyle, initStyle} from "_utils/rBem";
import {inject, observer} from "mobx-react";
import {RouterStore} from "mobx-react-router";
import * as React from "react";

@inject("routing")
@observer
export class HeaderComponent extends React.Component<{routing?: RouterStore}, {}> {

    constructor(props){
        super(props);
    }

    public render() {

        return (
            <div className={getStyle("top-heading")}>
                <div className={getStyle("top-heading", "currency-container")}>
                    <div className={getStyle("menu-button")}>
                        <div className={initStyle(getStyle("hamburger"))}>
                            <div className={getStyle("hamburger", "hamburger-stripe1")}>{}</div>
                            <div className={getStyle("hamburger", "hamburger-stripe2")}>{}</div>
                            <div className={getStyle("hamburger", "hamburger-stripe3")}>{}</div>
                        </div>
                    </div>
                    <div className={initStyle(getStyle("top-heading", "saved-tabs-wrapper"))}>{}</div>
                    <TopMenuComponent/>
                </div>
            </div>
        );
    }

}

export default HeaderComponent;
