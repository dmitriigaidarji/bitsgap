import {FooterSection, HeaderInfoBlock} from "_blocks";
import BackdropComponent from "_component/BackdropComponent";
import ErrorComponent from "_component/ErrorComponent";
import LazyLoadComponent from "_component/LazyLoadComponent";
import LoadingComponent from "_component/LoadingComponent";
import TermsAndConditionsStore from "_store/termsAndConditions";
import {getStyle} from "_utils/rBem";
import {Provider} from "mobx-react";
import * as React from "react";
import {Route, Switch} from "react-router";

declare const System: { import: (path: string) => Promise<any>; };

const TermsConditions = LazyLoadComponent(() => System.import("_page/TermsConditions"), LoadingComponent, ErrorComponent);

export class PageLayout extends React.Component<{}, {}> {

    constructor(props){
        super(props);
    }

    public render() {
        return [
            <HeaderInfoBlock key={"HeaderInfoBlock"}/>,
            (
                <section key={"contentBlock"} style={{minHeight: "70vh", paddingTop: "77px"}} className={getStyle("page-content")}>
                    <Provider termsAndConditions={TermsAndConditionsStore}>
                        <Switch>
                            <Route path="/terms-conditions" component={TermsConditions}/>
                        </Switch>
                    </Provider>
                </section>
            ),
            <BackdropComponent key={"BackdropComponent"} hideOnClick={true}/>,
            <FooterSection key={"FooterSection"}/>,
        ];
    }
}

export default PageLayout;
