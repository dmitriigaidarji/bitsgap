import ImageComponent from "_component/ImageComponent";
import {getStyle} from "_utils/rBem";
import * as React from "react";

export default class SignUp extends React.Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            userType: "personal",
        };
    }

    public handleUserTypeChange = (event) => {
        this.setState({userType: event.target.value});
    }

    public render() {
        const {userType} = this.state;
        const {history} = this.props;
        console.log(history)
        return (
            <div className={getStyle("signup")}>
                <div className={getStyle("signup", "menu-container")}>
                    <div className={getStyle("signup", "title")}>Signup for free!</div>
                    <div className={getStyle("signup", "subtitle")}>Trade on all crypto exchanges from one interface
                    </div>
                    <div className={getStyle("signup", "main-form")}>
                        <div className={getStyle("signup", "form-item")}>
                            <div
                                className={getStyle("signup", "left-radio")}
                                style={userType === "business" ? {opacity: 0.5} : {}}
                            >
                                <input
                                    onChange={this.handleUserTypeChange}
                                    type="radio"
                                    name="usertype"
                                    value="personal"
                                    checked={userType === "personal"}
                                />
                                <span className={getStyle("signup", "radio-label")}>Personal</span>
                            </div>
                            <div
                                className={getStyle("signup", "right-radio")}
                                style={userType === "personal" ? {opacity: 0.6} : {}}
                            >
                                <input
                                    onChange={this.handleUserTypeChange}
                                    type="radio"
                                    name="usertype"
                                    value="business"
                                    checked={userType === "business"}
                                />
                                <span className={getStyle("signup", "radio-label")}>Business</span>
                            </div>
                        </div>
                        <div className={getStyle("signup", "form-item")}>
                            <div className={getStyle("signup", "label")}>Username
                                <span className={getStyle("signup", "label-desc")}>(will be used to log in)</span>
                            </div>
                            <input className={getStyle("signup", "form-input")} name="username"/>
                        </div>
                        <div className={getStyle("signup", "form-item")}>
                            <div className={getStyle("signup", "label")}>Email address</div>
                            <input className={getStyle("signup", "form-input")} type="email" name="email"/>
                        </div>
                        <div className={getStyle("signup", "form-item")}>
                            <div className={getStyle("signup", "label")}>Create password</div>
                            <input className={getStyle("signup", "form-input")} type="password" name="password"/>
                        </div>
                        <div className={getStyle("signup", "form-info")}>
                            Password must contain a letter and a number, and be minimum of 8 characters
                        </div>
                        <div>
                            <div className={getStyle("signup", "label-out")}>Preferred FIAT currency</div>
                            <div className={getStyle("signup", "currency")}>
                                USD
                                <ImageComponent alt={"Choose Currency"} name={"arrow-thin-right"} type={"png"}/>
                            </div>
                        </div>
                    </div>
                    <div className={getStyle("signup", "agreeemail-container")}>
                        <div className={getStyle("signup", "checkbox-container")}>
                            <input type="checkbox" name="agreeemails"/>
                        </div>
                        <div className={getStyle("signup", "emails-agree")}>
                            I agree to receive educational and marketing emails about Bitsgap. (You can unsubscribe at
                            any time by using the link provided at the bottom of all our emails)
                        </div>
                    </div>
                    <div className={getStyle("signup", "submit-btn")}>
                        Sign up
                    </div>
                    <div className={getStyle("signup", "terms-confirm")}>
                        By clicking on “Sign up”, you confirm that you have read and accepted our
                        &nbsp;<span>Terms and Conditions</span> and <span>Privacy Policy</span>.
                    </div>
                    <div className={getStyle("signup", "login-container")}>
                        <span>Have an account?</span> <span className={getStyle("signup", "link")}>Sign in</span>
                    </div>
                </div>
                <div className={getStyle("signup", "cryptostats")}>
                    18 crypto exchanges | 7000+ Crypto Pairs | Demo Trading | Portfolio | 1-Click Arbitrage
                </div>
                <div
                    className={getStyle("signup", "closesign")}
                    onClick={() => {
                         history.push("/");
                     }}
                >
                    x
                </div>
            </div>
        );
    }
}
