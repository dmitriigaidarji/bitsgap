import {FooterSection, HeaderInfoBlock} from "_blocks";
import BackdropComponent from "_component/BackdropComponent";
import {IHomePage} from "_page/Home/interface";
import {inject, observer} from "mobx-react";
import * as React from "react";

@inject("navigation", "home", "routing")
@observer
export class HomePage extends React.Component<IHomePage, {}> {

    public render() {
        return (
            <div>
                <HeaderInfoBlock key={"HeaderInfoBlock"}/>
                {this.props.home.showBack && <div>
                    <FooterSection key={"FooterSection"}/>
                    <BackdropComponent key={"BackdropComponent"} hideOnClick={true}/>
                </div>}
            </div>
            );
    }
}

export default HomePage;
