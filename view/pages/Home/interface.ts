import {RouterStore} from "mobx-react-router";
import {ReactNode} from "react";
import {HomeStoreClass} from "_store/home";

export interface IHomePage {
    routing?: RouterStore;
    children?: ReactNode;
    home?: HomeStoreClass;
}
