import App from "_clientRouter";
import * as React from "react";
import {hydrate} from "react-dom";

if (process.env.NODE_ENV !== "production") {
    const {AppContainer} = require("react-hot-loader");

    const renderApplication = (Component: any) => {
        hydrate(
            <AppContainer>
                <Component/>
            </AppContainer>,
            document.getElementById("application"),
        );
    };
    renderApplication(App);

    if (module.hot) {
        module.hot.accept("_clientRouter", () => {
            const NewApp = require("_clientRouter").default;
            renderApplication(NewApp);
        });
    }
} else {
    hydrate(
        <App/>,
        document.getElementById("application"),
    );
}
