import ErrorComponent from "_component/ErrorComponent";
import LazyLoadComponent from "_component/LazyLoadComponent";
import LoadingComponent from "_component/LoadingComponent";
import Home from "_page/Home";
import SignUp from "_page/SignUp";
import HomeStore from "_store/home";
import Navigation from "_store/navigation";
import routingStore from "_store/routing";
import {Provider} from "mobx-react";
import * as React from "react";
import {Route, Switch} from "react-router";

declare const System: { import: (path: string) => Promise<any>; };

// const Home = LazyLoadComponent(() => System.import("_page/Home"), LoadingComponent, ErrorComponent);
const PageLayout = LazyLoadComponent(() => System.import("_component/PageLayout"), LoadingComponent, ErrorComponent);
const BlankLayout = LazyLoadComponent(() => System.import("_component/BlankLayout"), LoadingComponent, ErrorComponent);

export const Routes = () => (
    <Provider navigation={Navigation} home={HomeStore} routing={routingStore}>
        <Switch>
            <Route exact={true} path="/" component={Home}/>
            <Route path="/security" component={BlankLayout}/>
            <Route path="/sign-up" component={SignUp} />
            <Route path="/terms-conditions" component={PageLayout}/>
            <Route path="/draft" component={PageLayout}/>
            <Route path="/" component={Home}/>
        </Switch>
    </Provider>
);

export default Routes;
